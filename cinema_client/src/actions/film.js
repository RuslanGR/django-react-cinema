import * as filmTypes from '../types/film';
import api from '../api';

const filmAddedOne = film => ({
  type: filmTypes.FILM_FETCH_ONE,
  payload: film,
});

const filmAddedAll = (films, next, prev) => ({
  type: filmTypes.FILM_FETCH_ALL,
  payload: {
    films,
    next,
    prev,
  },
});

const sessionAddedAll = sessions => ({
  type: filmTypes.SESSION_FETCH_ALL,
  payload: sessions,
});

const ordersAdded = orders => ({
  type: filmTypes.SESSION_FETCH_ONE,
  payload: orders,
});

export const sessionAdded = session => ({
  type: filmTypes.SESSION_ADD_CURRENT,
  payload: session,
});

// THUNKS

export const filmFetchOne = id => dispatch =>
  api.film
    .fetchOne(id)
    .then(res => dispatch(filmAddedOne(res)))
    .catch(err => err.message);

export const filmFetchAll = url => dispatch =>
  api.film
    .fetchAll(url)
    .then(res => dispatch(filmAddedAll(res.results, res.next, res.previous)))
    .catch(err => err.message);

export const sessionFetchAll = id => dispatch =>
  api.film.fetchSessionAll(id).then(res => dispatch(sessionAddedAll(res)));

export const fetchSessionOrders = id => dispatch =>
  api.film.fetchSessionOrders(id).then(res => {
    dispatch(sessionAdded({ price: res.price }));
    return dispatch(ordersAdded(res.items));
  });
