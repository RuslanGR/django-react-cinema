import jwtDecode from 'jwt-decode';

import * as userTypes from '../types/user';
import api from '../api';
import setAuthHeaders from '../utils/setAuthHeaders';

export const userLoggedIn = user => ({
  type: userTypes.USER_LOGIN,
  payload: user,
});

export const userLoggedOut = () => ({
  type: userTypes.USER_LOGOUT,
});

// Thunks
export const logout = () => dispatch => {
  localStorage.removeItem('CinemaToken');
  dispatch(userLoggedOut());
  setAuthHeaders();
};

export const login = credentials => dispatch => {
  api.user.login(credentials).then(res => {
    setAuthHeaders(res.access);
    const payload = jwtDecode(res.access);
    localStorage.CinemaToken = res.access;
    dispatch(userLoggedIn({ token: res.access, username: payload.username }));
  });
};

export const signup = credentials => dispatch => {
  api.user.signup(credentials).then(res => {
    dispatch(
      login({
        username: res.username,
        password: credentials.password,
      })
    );
  });
};
