export const FILM_FETCH_ONE = 'FILM_FETCH_ONE';
export const FILM_FETCH_ALL = 'FILM_FETCH_ALL';

export const SESSION_FETCH_ALL = 'SESSION_FETCH_ALL';
export const SESSION_FETCH_ONE = 'SESSION_FETCH_ONE';
export const SESSION_ADD_CURRENT = 'SESSION_ADD_CURRENT';
