import React from 'react';
import { Route, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Footer from './common/Footer';
import Navbar from './common/Navbar/Navbar';
import HomePage from './pages/HomePage/HomePage';
import FilmPage from './pages/FilmPage/index';
import LoginPage from './pages/LoginPage';
import ContactsPage from './pages/ContactsPage';
import AboutUsPage from './pages/AboutUsPage';
import LogoutPage from './pages/LogoutPage';
import SignupPage from './pages/SignupPage';
import SessionPage from './pages/SessionPage/SessionPage';

const App = ({ location, isAuthenticated, username }) => (
  <div>
    <div className="container min-vh-100">
      <Navbar authenticated={isAuthenticated} username={username} />
      <Route location={location} path="/" exact component={HomePage} />
      <Route location={location} path="/films/:id" exact component={FilmPage} />
      <Route location={location} path="/contacts" component={ContactsPage} />
      <Route location={location} path="/about" component={AboutUsPage} />
      <Route path="/films/:id/session/:sessionId" component={SessionPage} />

      <Route location={location} path="/signup" component={SignupPage} />
      <Route location={location} path="/logout" component={LogoutPage} />
      <Route location={location} path="/login" component={LoginPage} />
    </div>
    <Footer />
  </div>
);

App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  username: PropTypes.string.isRequired,
};

const mapStateToProps = state => {
  return {
    isAuthenticated: !!state.user.token,
    username: state.user.username,
  };
};

export default withRouter(connect(mapStateToProps)(App));
