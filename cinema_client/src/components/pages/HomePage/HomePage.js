import React from 'react';

import FilmsContainer from './FilmsContainer/index';

const HomePage = () => (
  <div className="col-8 offset-2">
    <FilmsContainer />
  </div>
);

export default HomePage;
