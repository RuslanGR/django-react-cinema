import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './films_container.css';
import * as actions from '../../../../actions/film';
import FilmPreview from '../../../common/FilmPreview/index';
import Pagination from '../../../common/Pagination';
import CatPreloader from '../../../common/preloaders/CatPreloader';

class FilmsContainer extends Component {
  state = {
    loading: false,
  };

  componentWillMount() {
    this.setState({ loading: true });
    const { filmFetchAll, curr } = this.props;
    filmFetchAll(curr).finally(() => this.setState({ loading: false }));
  }

  onClick = e => {
    let nextUrl;
    e.preventDefault();
    this.setState({ loading: true });

    window.scrollTo(0, 0);

    if (e.target.name === 'next') {
      nextUrl = this.props.next;
    } else if (e.target.name === 'prev') {
      nextUrl = this.props.prev;
    }

    this.props
      .filmFetchAll(nextUrl)
      .finally(() => this.setState({ loading: false }));
  };

  render() {
    const { film, next, prev } = this.props;
    const { loading } = this.state;
    const Films = () => (
      <div>
        {film.map(item => (
          <FilmPreview key={item.id} film={item} />
        ))}
        <Pagination next={next} prev={prev} onClick={this.onClick} />
      </div>
    );

    return <div>{loading ? <CatPreloader /> : <Films />}</div>;
  }
}

FilmsContainer.propTypes = {
  film: PropTypes.shape({}).isRequired,
  next: PropTypes.string.isRequired,
  prev: PropTypes.string.isRequired,
  curr: PropTypes.string.isRequired,
  filmFetchAll: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    film: state.film.previews,
    next: state.film.next,
    prev: state.film.prev,
    curr: state.film.curr,
  };
};

export default connect(
  mapStateToProps,
  {
    filmFetchAll: actions.filmFetchAll,
  }
)(FilmsContainer);
