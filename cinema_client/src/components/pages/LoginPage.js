import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import * as actions from '../../actions/user';
import LoginForm from '../forms/LoginForm';

const LoginPage = ({ isAuthenticated, login }) =>
  !isAuthenticated ? (
    <div className="container">
      <div className="col-4 offset-4">
        <LoginForm onSubmit={login} />
      </div>
    </div>
  ) : (
    <Redirect to="/" />
  );

LoginPage.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.user.token,
  };
};

export default connect(
  mapStateToProps,
  { login: actions.login }
)(LoginPage);
