import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { signup } from '../../actions/user';
import SignupForm from '../forms/SignupForm';

const SignupPage = ({ isAuthenticated, signup }) =>
  !isAuthenticated ? (
    <div className="container">
      <div className="col-4 offset-4">
        <SignupForm onSubmit={signup} />
      </div>
    </div>
  ) : (
    <Redirect to="/" />
  );

const mapStateToProps = state => {
  return {
    isAuthenticated: state.user.token,
  };
};

SignupPage.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  signup: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  { signup }
)(SignupPage);
