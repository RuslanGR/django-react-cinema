import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as actions from '../../../actions/film';
import Preloader from '../../common/preloaders/Preloader/index';
import Sessions from '../../common/Sessions/index';
import './film_page.css';

class FilmPage extends Component {
  state = {
    loading: true,
  };

  componentWillMount = () => {
    window.scrollTo(0, 0);
    this.setState({ loading: true });

    const { sessionFetchAll, filmFetchOne } = this.props;
    const { id } = this.props.match.params;
    filmFetchOne(id).then(this.setState({ loading: false }));

    sessionFetchAll(id);
  };

  render() {
    const { film, sessions } = this.props;
    const { id } = this.props.match.params;
    const { loading } = this.state;

    const Card = () => (
      <div className="container">
        <div className="row">
          <div className="col-6 offset-4">
            <h1>{film.name}</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-4 sidebar">
            <img src={film.poster} className="film_picture" alt="poster" />
          </div>
          <div className="col-8 content">
            <p>{film.description}</p>
            <div className="mt-5">
              <Sessions sessions={sessions} filmId={id} />
            </div>
          </div>
        </div>
      </div>
    );

    return <div>{loading ? <Preloader /> : <Card />}</div>;
  }
}

FilmPage.propTypes = {
  film: PropTypes.shape({}).isRequired,
  sessions: PropTypes.shape([]).isRequired,
  filmFetchOne: PropTypes.func.isRequired,
  sessionFetchAll: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({ id: PropTypes.number.isRequired }).isRequired,
  }).isRequired,
};

const mapStateToProps = state => {
  return {
    film: state.film.current.film,
    sessions: state.film.current.sessions,
  };
};

export default connect(
  mapStateToProps,
  {
    filmFetchOne: actions.filmFetchOne,
    sessionFetchAll: actions.sessionFetchAll,
  }
)(FilmPage);
