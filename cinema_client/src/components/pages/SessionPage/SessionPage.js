import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import { connect } from 'react-redux';

import Hall from './Hall';
import Row from './Row/Row';
import './session.css';
import * as actions from '../../../actions/film';

class SessionPage extends React.Component {
  state = {
    seats: [],
    price: 0,
  };

  componentDidMount() {
    const { sessionId } = this.props.match.params;
    this.props.fetchSessionOrders(sessionId).then(res => {
      res.payload.forEach(item => {
        item.places.forEach(place =>
          $(`#pos_${item.row}_${place.place}`).addClass('place_taken')
        );
      });
    });
  }

  addSeat = data => {
    const seats = this.state.seats.slice();

    if (seats.length < 4) {
      seats.push(data);
      this.setState((state, props) => {
        return { seats, price: state.price + parseFloat(props.price) };
      });
    }
  };

  buyClick = () => {
    console.log(this.state.price);
  };

  render() {
    const { seats } = this.state;
    const rows = [];
    for (let i = 1; i <= 8; i += 1) {
      rows.push(
        <Row row={i} addSeat={this.addSeat} allow={seats.length < 4} />
      );
    }

    const orders = seats.map(item => (
      <li className="list-group-item bg-dark text-white mx-1 rounded d-flex flex-column">
        <i className="fas fa-times justify-content-end py-1 pointer" />
        <p>
          Ряд {item.row}, Место {item.place}
        </p>
        <p>Цена: {this.props.price}р.</p>
      </li>
    ));

    return (
      <Hall rows={rows} buyClick={this.buyClick} price={this.state.price} >
        <ul className="list-group flex-row">{orders}</ul>
      </Hall>
    );
  }
}

SessionPage.propTypes = {
  fetchSessionOrders: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired,
      sessionId: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
  price: PropTypes.number.isRequired,
};

function mapStateToProps(state) {
  return {
    price: state.film.current.session.price,
    rows: state.film.current.orders,
  };
}

export default connect(
  mapStateToProps,
  {
    fetchSessionOrders: actions.fetchSessionOrders,
  }
)(SessionPage);
