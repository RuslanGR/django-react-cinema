import React from 'react';
import PropTypes from 'prop-types';
import './row.css';
import Place from './Place';

const Row = ({ row, addSeat, allow }) => {
  const places = [];
  for (let i = 1; i <= 20; i += 1) {
    places.push(
      <Place
        id={`pos_${row}_${i}`}
        place={i}
        row={row}
        addSeat={addSeat}
        allow={allow}
      />
    );
  }

  return <div className="row">{places}</div>;
};

Row.propTypes = {
  addSeat: PropTypes.func.isRequired,
  row: PropTypes.number.isRequired,
  allow: PropTypes.bool.isRequired,
};

export default Row;
