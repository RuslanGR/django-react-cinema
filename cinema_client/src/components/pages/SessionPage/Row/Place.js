import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import $ from 'jquery';

class Place extends React.Component {
  state = {
    statuses: ['free', 'taken', 'your'],
    status: 'free',
  };

  componentDidMount() {
    if ($(`#${this.id}`).hasClass('place_taken')) {
      this.setState({ status: 'taken' });
    }
  }

  onClick = () => {
    const { place, row, allow } = this.props;

    if (!allow) return;
    if (!this.placeTaken) {
      this.props.addSeat({ place, row });
    }
    if (this.state.status === 'free') {
      this.setState({ status: 'your' });
    } else if (this.state.status === 'your') {
      this.setState({ status: 'free' });
    }
  };

  render() {
    const { place, row } = this.props;
    this.id = `pos_${row}_${place}`;
    this.placeTaken = $(`#${this.id}`).hasClass('place_taken');
    const className = classNames({
      place: true,
      place_your: this.state.status === 'your' && !this.placeTaken,
      place_taken: this.placeTaken,
    });

    return (
      <div
        id={this.id}
        className={className}
        onClick={this.onClick}
        role="button"
      >
        {this.state.status === 'your' && !this.placeTaken && <p>{place}</p>}
      </div>
    );
  }
}

Place.propTypes = {
  addSeat: PropTypes.func.isRequired,
  row: PropTypes.number,
  place: PropTypes.number,
  allow: PropTypes.bool.isRequired,
};

Place.defaultProps = {
  row: 0,
  place: 0,
  taken: false,
};

export default Place;
