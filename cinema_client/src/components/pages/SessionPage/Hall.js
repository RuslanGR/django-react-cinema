import React from 'react';
import PropTypes from 'prop-types';

const Hall = ({rows, children, price, buyClick}) => (
  <div className="container">
    <div className="row justify-content-center mt-5">
      {rows}
      <div className="monitor" />
    </div>
    <div className="row py-5">
      <div className="col-2">
        {price !== 0 && (
          <button className="btn btn-primary btn-lg" onClick={buyClick}>
            {price} руб.
          </button>
        )}
      </div>
      <div className="col-9">
        <div className="col">
          {children}
        </div>
      </div>
    </div>
  </div>
);

Hall.propTypes = {
  buyClick: PropTypes.func.isRequired,
  price: PropTypes.number.isRequired,
  rows: PropTypes.shape([]).isRequired,
  children: PropTypes.shape([]).isRequired
};

export default Hall;
