import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as actions from '../../actions/user';

const LogoutPage = ({ logout }) => {
  logout();
  return (
    <div className="py-2">
      <h2>Благодарим вас за время, проведенное на этом сайте.</h2>
      <Link to="/login" className="nav-link">
        Log in
      </Link>
      <Link to="/" className="nav-link">
        Home
      </Link>
    </div>
  );
};

LogoutPage.propTypes = {
  logout: PropTypes.func.isRequired,
};

export default connect(
  null,
  { logout: actions.logout }
)(LogoutPage);
