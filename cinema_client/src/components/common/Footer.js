import React from 'react';
import { Link } from 'react-router-dom';

const onClick = () => {
  window.scrollTo(0, 0);
};

const Footer = () => (
  <footer className="page-footer font-small bg-light pt-4">
    <div className="container">
      <ul className="list-unstyled list-inline text-center py-2">
        <li className="list-inline-item">
          <h5 className="mb-1">Register for free</h5>
        </li>
        <li className="list-inline-item">
          <Link to="/signup" onClick={onClick} className="btn btn-outline-dark">
            Sign up!
          </Link>
        </li>
      </ul>
    </div>

    <div className="footer-copyright text-center py-3">
      © 2018 Copyright:
      <Link to="/">Dasha cinema</Link>
    </div>
  </footer>
);

export default Footer;
