import React from 'react';

import './preloader.css';

const Preloader = () => <div id="loader" />;

export default Preloader;
