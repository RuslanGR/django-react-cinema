import React from 'react';
import { NavLink as Link, Link as PrimaryLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import Authorized from './Authorized';
import Guest from './Guest';
import './navbar.css';

const Navbar = ({ authenticated, username }) => {
  return (
    <div className="container py-3" id="navigation">
      <div className="row">
        <div className="col">
          <nav className="nav justify-content-start">
            <PrimaryLink className="nav-link text-uppercase" to="/">
              Dasha cinema
            </PrimaryLink>
          </nav>
        </div>
        <div className="col">
          <nav className="nav justify-content-center">
            <Link
              className="nav-link link"
              to="/"
              exact
              activeClassName="active"
            >
              All films
            </Link>
            <Link
              className="nav-link link"
              to="/about"
              activeClassName="active"
            >
              About us
            </Link>
            <Link
              className="nav-link link"
              to="/contacts"
              exact
              activeClassName="active"
            >
              Contacts
            </Link>
          </nav>
        </div>
        <div className="col">
          {authenticated ? <Authorized username={username} /> : <Guest />}
        </div>
      </div>
    </div>
  );
};

Navbar.propTypes = {
  authenticated: PropTypes.bool.isRequired,
  username: PropTypes.string.isRequired,
};

export default Navbar;
