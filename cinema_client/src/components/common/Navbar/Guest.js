import React from 'react';
import { NavLink as Link } from 'react-router-dom';

const Guest = () => (
  <nav className="nav justify-content-end">
    <Link className="nav-link link" exact activeClassName="active" to="/login">
      Log in
    </Link>
    <Link className="nav-link link" exact activeClassName="active" to="/signup">
      Sign up
    </Link>
  </nav>
);

export default Guest;
