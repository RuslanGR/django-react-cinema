import React from 'react';
import PropTypes from 'prop-types';
import { NavLink as Link } from 'react-router-dom';

const Authorized = ({ username }) => (
  <nav className="nav justify-content-end">
    <p className="nav-link">{username}</p>
    <div className="dropdown show">
      <button
        className="btn btn-secondary dropdown-toggle"
        type="button"
        id="dropdownMenuButton"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        Profile
      </button>

      <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <Link className="nav-link" to="/logout">
          Log out
        </Link>
      </div>
    </div>
  </nav>
);

Authorized.propTypes = {
  username: PropTypes.string.isRequired,
};

export default Authorized;
