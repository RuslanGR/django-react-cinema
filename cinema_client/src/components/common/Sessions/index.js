import React from 'react';
import PropTypes from 'prop-types';

import ModalButton from './ModalButton';
import './ModalButton/sessions.css';
import Modal from './ModalButton/Modal';

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const Try = ({ sessions, filmId }) => (
  <div>
    {sessions.map(session => {
      const sessionDay = new Date(session.date);
      return (
        <div>
          <p>
            {sessionDay.getDate()} {monthNames[sessionDay.getMonth()]}
          </p>
          <div className="session-container">
            {session.sessions &&
              session.sessions.map(sess => (
                <ModalButton date={sess.time} id={sess.id} filmId={filmId} sessionPrice={sess.price} />
              ))}
          </div>
          <Modal />
        </div>
      );
    })}
  </div>
);

Try.propTypes = {
  sessions: PropTypes.shape([]).isRequired,
  filmId: PropTypes.number.isRequired,
};

export default Try;
