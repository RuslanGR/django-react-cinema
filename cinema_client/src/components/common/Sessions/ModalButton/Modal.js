import React from "react";
// import PropTypes from "prop-types"

const Modal = () => (
  <div
    className="modal fade"
    id="exampleModal"
    tabIndex="-1"
    role="dialog"
    aria-labelledby="exampleModalLabel"
    aria-hidden="true"
  >
    <div className="modal-dialog modal-lg" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">
            Modal title
          </h5>
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate dolorem exercitationem explicabo fuga hic laborum libero maxime molestias, nulla optio perferendis placeat quo sed sequi voluptatem? Consectetur cum nemo officia.</p>
              </div>
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-secondary"
            data-dismiss="modal"
          >
            Close
          </button>
        </div>
      </div>
    </div>
  </div>
);

Modal.propTypes = {
};


export default Modal;
