import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const ModalButton = ({ date, id, filmId }) => {
  const time = new Date(date);
  return (
    <div>
      <Link
        to={`/films/${filmId}/session/${id}`}
        key={id}
        className="session_link"
      >
        <div className="session-card">
          {`${time.getHours()}:${time.getMinutes()}`}
        </div>
      </Link>
    </div>
  );
};

ModalButton.propTypes = {
  date: PropTypes.shape({}).isRequired,
  id: PropTypes.number.isRequired,
  filmId: PropTypes.number.isRequired,
};

export default ModalButton;
