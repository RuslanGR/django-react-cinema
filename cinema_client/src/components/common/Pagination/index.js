import React from 'react';
import PropTypes from 'prop-types';
import './pagination.css';

const Pagination = ({ next, prev, onClick }) => (
  <div className="row pagination py-4">
    {prev && (
      <div className="col">
        <button
          name="prev"
          className="float-left pagination-button"
          onClick={onClick}
        >
          Предыдущая
        </button>
      </div>
    )}
    {next && (
      <div className="col">
        <button
          name="next"
          className="pr-1 float-right pagination-button"
          onClick={onClick}
        >
          Следующая
        </button>
      </div>
    )}
  </div>
);

Pagination.propTypes = {
  next: PropTypes.string.isRequired,
  prev: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Pagination;
