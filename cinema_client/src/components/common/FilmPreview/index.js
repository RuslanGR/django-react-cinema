import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import './film_preview.css';

const FilmPreview = ({ film }) => (
  <Link to={`/films/${film.id}`} className="film-container__link">
    <div className="film-container">
      <div className="row">
        <div className="col-4">
          <img className="film-container__image" src={film.poster} alt="" />
        </div>
        <div className="col-8 align-content-around">
          <h3 className="pt-3">{film.name}</h3>
          <p>{film.description}</p>
          <span className="align-bottom">
            Продолжительность {film.duration}
          </span>
        </div>
      </div>
    </div>
  </Link>
);

FilmPreview.propTypes = {
  film: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    duration: PropTypes.string.isRequired,
  }).isRequired,
};

export default FilmPreview;
