import React, { Component } from 'react';
import PropTypes from 'prop-types';

class LoginForm extends Component {
  state = {
    data: {
      username: '',
      password: '',
    },
    errors: {},
    loading: false,
  };

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value },
    });

  onSubmit = e => {
    e.preventDefault();
    this.props.onSubmit(this.state.data);
  };

  render() {
    return (
      <form method="post" onSubmit={this.onSubmit} className="py-4">
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input
            onChange={this.onChange}
            type="text"
            name="username"
            className="form-control"
            value={this.state.username}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            onChange={this.onChange}
            type="password"
            name="password"
            className="form-control"
            value={this.state.password}
          />
        </div>
        <button className="btn btn-primary">Log in</button>
      </form>
    );
  }
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default LoginForm;
