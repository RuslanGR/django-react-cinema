import React, { Component } from 'react';
import PropTypes from 'prop-types';

class LoginForm extends Component {
  state = {
    data: {
      username: '',
      password: '',
      password2: '',
      email: '',
    },
    errors: {},
    loading: false,
  };

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value },
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);

    if (Object.keys(errors).length === 0) {
      delete this.state.data.password2;
      this.props.onSubmit(this.state.data);
    }
  };

  validate = data => {
    const errors = {};
    if (data.password !== data.password2)
      errors.password = 'Passwords must be similar.';
    if (data.username === '') errors.username = 'Username is required!';
    return errors;
  };

  render() {
    return (
      <form method="post" onSubmit={this.onSubmit} className="py-4">
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input
            onChange={this.onChange}
            type="text"
            name="username"
            className="form-control"
            value={this.state.username}
          />
        </div>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            onChange={this.onChange}
            type="text"
            name="email"
            className="form-control"
            value={this.state.email}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            onChange={this.onChange}
            type="password"
            name="password"
            className="form-control"
            value={this.state.password}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password2">Confirm password</label>
          <input
            onChange={this.onChange}
            type="password"
            name="password2"
            className="form-control"
            value={this.state.password2}
          />
        </div>
        <button className="btn btn-primary">Sign up!</button>
      </form>
    );
  }
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default LoginForm;
