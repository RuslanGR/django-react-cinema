import * as filmTypes from '../types/film';

const initialState = {
  previews: [],
  next: '',
  prev: '',
  curr: 'http://127.0.0.1:8000/api/cinema/films/',
  current: {
    film: {},
    sessions: [],
    session: {},
    orders: []
  },
};

const filmReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case filmTypes.SESSION_ADD_CURRENT:
      return {
        ...state,
        current: {
          ...state.current,
          session: action.payload,
        },
      };
    case filmTypes.SESSION_FETCH_ONE:
      return {
        ...state,
        current: {
          ...state.current,
          orders: action.payload,
        },
      };
    case filmTypes.SESSION_FETCH_ALL:
      return {
        ...state,
        current: {
          ...state.current,
          sessions: action.payload,
        },
      };
    case filmTypes.FILM_FETCH_ALL:
      return {
        ...state,
        next: action.payload.next,
        prev: action.payload.prev,
        previews: action.payload.films,
      };
    case filmTypes.FILM_FETCH_ONE:
      return {
        ...state,
        current: {
          ...state.current,
          film: action.payload,
        },
      };
    default:
      return state;
  }
};

export default filmReducer;
