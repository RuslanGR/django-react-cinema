import { combineReducers } from 'redux';

import userReducer from './user';
import filmReducer from './film';

const rootReducer = combineReducers({
  user: userReducer,
  film: filmReducer,
});

export default rootReducer;
