import * as userTypes from '../types/user';

const initialState = {
  token: '',
  username: '',
};

const userReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case userTypes.USER_LOGIN:
      return {
        ...action.payload,
      };
    case userTypes.USER_LOGOUT:
      return {};
    default:
      return state;
  }
};

export default userReducer;
