import axios from 'axios';

const setAuthHeaders = (token = null) => {
  if (token) {
    axios.defaults.headers.common.Authorization = `Token ${token}`;
  } else {
    delete axios.defaults.headers.common.Authorization;
  }
};

export default setAuthHeaders;
