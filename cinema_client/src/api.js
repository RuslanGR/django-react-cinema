import axios from 'axios';

const baseUrl = 'http://127.0.0.1:8000';

export default {
  user: {
    login: credentials =>
      axios
        .post(`${baseUrl}/api/token/`, { ...credentials })
        .then(response => response.data),
    signup: credentials =>
      axios
        .post(`${baseUrl}/api/signup/`, { ...credentials })
        .then(response => response.data),
  },
  film: {
    fetchAll: url => axios.get(url).then(response => response.data),
    fetchOne: id =>
      axios
        .get(`${baseUrl}/api/cinema/films/${id}/`)
        .then(response => response.data),
    fetchSessionAll: id =>
      axios
        .get(`${baseUrl}/api/cinema/sessions/${id}/`)
        .then(response => response.data),
    fetchSessionOrders: id =>
      axios
        .get(`${baseUrl}/api/cinema/session/${id}/orders/`)
        .then(response => response.data),
  },
};
