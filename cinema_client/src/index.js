import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import jwtDecode from 'jwt-decode';
import 'bootstrap/dist/css/bootstrap.min.css';

import { userLoggedIn, logout } from './actions/user';
import setAuthHeaders from './utils/setAuthHeaders';
import store from './store';
import App from './components/App';

if (localStorage.CinemaToken) {
  const token = localStorage.CinemaToken;
  const payload = jwtDecode(token);
  const now = Math.ceil(Date.now() / 1000);
  if (payload.exp < now) {
    store.dispatch(logout());
  } else {
    setAuthHeaders(token);

    const user = {
      username: payload.username,
      token,
    };
    store.dispatch(userLoggedIn(user));
  }
}

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Route component={App} />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
