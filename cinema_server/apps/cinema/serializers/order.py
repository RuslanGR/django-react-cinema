from rest_framework.serializers import ModelSerializer

from ..models import Order


class OrderSerializer(ModelSerializer):
    """Order serializer"""

    class Meta:
        """Meta"""

        model = Order
        fields = ('id', 'place', 'phone')
