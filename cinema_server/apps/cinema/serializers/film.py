from rest_framework.serializers import ModelSerializer

from ..models import Film


class FilmSerializer(ModelSerializer):
    """Film serializer"""

    class Meta:
        """Meta"""

        model = Film
        fields = '__all__'
