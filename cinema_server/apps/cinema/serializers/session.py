from rest_framework.serializers import ModelSerializer

from ..models import Session


class SessionSerializer(ModelSerializer):
    """Session serializer"""

    class Meta:
        """Meta"""

        model = Session
        fields = '__all__'
