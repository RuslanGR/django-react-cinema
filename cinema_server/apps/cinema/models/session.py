from django.db import models
from datetime import datetime

from .film import Film


class Session(models.Model):
    """Session model"""

    film = models.ForeignKey(Film, verbose_name='Фильм', on_delete=models.CASCADE)
    time = models.DateTimeField(verbose_name='Время сеанса')
    price = models.DecimalField(verbose_name='Цена', decimal_places=2, max_digits=6)
    max_seats = models.PositiveIntegerField(verbose_name='Количество мест', default=100)

    def __str__(self):
        time = datetime.strftime(self.time, "%Y.%m.%d %H:%M")
        return f'{self.pk} {self.film} {time}'

    class Meta:
        """Meta"""

        verbose_name = 'Сеанс'
        verbose_name_plural = 'Сеансы'
