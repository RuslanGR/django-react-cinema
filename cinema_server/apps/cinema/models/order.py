from django.db import models

from .session import Session


class Order(models.Model):
    """Order model"""

    row = models.PositiveIntegerField(verbose_name='Ряд')
    place = models.PositiveIntegerField(verbose_name='Место')
    phone = models.CharField(verbose_name='Мобильный номер покупателя', max_length=10)
    session = models.ForeignKey(Session, verbose_name='Сеанс', on_delete=models.CASCADE)
    purchased = models.BooleanField(verbose_name='Куплен', default=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        taken_seats = Order.objects.filter(session=self.session).count()
        if taken_seats < self.session.max_seats:
            super().save()

    def __str__(self):
        return f'Session {self.session} {self.phone}'

    class Meta:
        """Meta"""

        verbose_name = 'Билет'
        verbose_name_plural = 'Билеты'
