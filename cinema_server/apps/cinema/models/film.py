from datetime import timedelta

from django.db import models


class Film(models.Model):
    """Film model"""

    name = models.CharField(verbose_name='Название фильма', max_length=128)
    description = models.TextField(verbose_name='Описание')
    is_active = models.BooleanField(verbose_name='В прокате')
    duration = models.DurationField(verbose_name='Продолжительность', default=timedelta(hours=2))
    poster = models.ImageField(verbose_name='Изображение', upload_to='cinema', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        """Meta"""

        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильмы'
