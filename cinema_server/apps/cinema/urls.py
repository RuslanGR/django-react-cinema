from django.urls import path

from .views import (
    FilmList,
    FilmDetail,

    SessionList,
    OrderList,
)


urlpatterns = (
    path('films/', FilmList.as_view()),
    path('films/<int:pk>/', FilmDetail.as_view()),

    path('sessions/<int:pk>/', SessionList.as_view()),
    path('session/<int:pk>/orders/', OrderList.as_view()),
)
