from django.contrib import admin
from datetime import datetime


class SessionAdmin(admin.ModelAdmin):
    """Session admin"""

    def time_(self, obj):
        return datetime.strftime(obj.time, "%Y.%m.%d %H:%M")
    time_.short_description = 'Время'

    list_display = ('id', 'film', 'time_', 'price')
    list_per_page = 20
