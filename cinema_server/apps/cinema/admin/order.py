from django.contrib import admin


class OrderAdmin(admin.ModelAdmin):
    """Order admin"""

    list_display = ('session', 'row', 'place', 'purchased')
    list_per_page = 20
