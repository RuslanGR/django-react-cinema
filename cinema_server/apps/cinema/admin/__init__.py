"""Admin"""

from django.contrib import admin

from ..models import Session, Film, Order
from .session import SessionAdmin
from .order import OrderAdmin


admin.site.register(Order, OrderAdmin)
admin.site.register(Session, SessionAdmin)
admin.site.register(Film)
