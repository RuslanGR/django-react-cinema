from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny

from ..serializers import UserSerializer


class RegistrationView(CreateAPIView):
    """Registration view"""

    permission_classes = (AllowAny,)
    serializer_class = UserSerializer
