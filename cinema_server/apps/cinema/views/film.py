from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from apps.cinema.models import Film
from apps.cinema.serializers import FilmSerializer


class FilmList(ListCreateAPIView):
    """Film list view"""

    permission_classes = (AllowAny,)
    serializer_class = FilmSerializer

    def get_queryset(self):
        # return Film.objects.filter(is_active=True)
        return Film.objects.all()

    def get_permissions(self):
        """Get permissions"""
        if self.request.method == 'POST':
            permission_classes = [IsAdminUser]
        elif self.request.method == 'GET':
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAdminUser]

        return [permission() for permission in permission_classes]


class FilmDetail(RetrieveUpdateDestroyAPIView):
    """Film detail view"""

    permission_classes = (AllowAny,)
    serializer_class = FilmSerializer
    queryset = Film.objects.all()

    def get_permissions(self):
        """Get permissions"""
        if self.request.method == 'POST':
            permission_classes = [IsAdminUser]
        elif self.request.method == 'GET':
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAdminUser]

        return [permission() for permission in permission_classes]
