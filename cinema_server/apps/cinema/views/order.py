from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAdminUser

from ..models import Order, Session
from ..serializers import OrderSerializer


class OrderList(ListCreateAPIView):
    """Order list"""

    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def get_permissions(self):
        """Get permissions"""
        if self.request.method == 'POST':
            permission_classes = [IsAdminUser]
        elif self.request.method == 'GET':
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAdminUser]

        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        """List"""
        session_id = kwargs.get('pk')
        session = Session.objects.get(id=session_id)
        orders = Order.objects.filter(session__id=session_id)

        rows = [order.row for order in orders]
        results = []
        for row in set(rows):
            seats = orders.filter(row=row)
            results.append({
                'row': row,
                'places': self.serializer_class(seats, many=True).data
            })

        return Response({'price': session.price, 'items': results})
