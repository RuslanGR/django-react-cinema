from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.response import Response

from ..models import Session
from ..serializers import SessionSerializer


class SessionList(ListCreateAPIView):
    """Session List View"""

    serializer_class = SessionSerializer
    queryset = Session.objects.all()

    def get_permissions(self):
        """Get permissions"""
        if self.request.method == 'POST':
            permission_classes = [IsAdminUser]
        elif self.request.method == 'GET':
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAdminUser]

        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        """Output array or sessions"""
        film_id = kwargs.get('pk')
        session_objects = Session.objects.filter(film_id__exact=film_id)

        a = [obj.time.date() for obj in session_objects]
        results = []
        for time in set(a):
            session_list = session_objects.filter(time__date=time)
            results.append({
                'date': str(time),
                'sessions': self.serializer_class(session_list, many=True).data
            })

        return Response(results)
